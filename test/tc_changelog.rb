# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/changelog'

# Test class for changelog
class TestGit < Test::Unit::TestCase
  def test_changelog
    log = ChangeLog.new('v1.1.1', '2020-06-31')
    log.add_entry(ClogCmdCode::CLOG_ADDED, 'msg')
    log.add_entry(ClogCmdCode::CLOG_REMOVED, 'removed component x')
    log.add_entry(ClogCmdCode::CLOG_CHANGED, 'changed file 1')
    log.add_entry(ClogCmdCode::CLOG_CHANGED, 'changed file 2')
    log.add_entry(ClogCmdCode::CLOG_FIXED, 'fixed critical bug 1')
    log.add_entry(ClogCmdCode::CLOG_FIXED, 'fixed critical bug 2')
    expect = <<~STR
      #### [v1.1.1] 2020-06-31
      ##### Added
      - msg
      ##### Changed
      - changed file 1
      - changed file 2
      ##### Fixed
      - fixed critical bug 1
      - fixed critical bug 2
      ##### Removed
      - removed component x
    STR

    assert { log.render_to_md == expect }
  end
end
