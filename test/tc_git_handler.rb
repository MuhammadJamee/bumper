# frozen_string_literal: true

require 'test/unit'
require 'assert2'
require 'semantic'
require_relative '../lib/git_handler'

# Test class for git classes
class TestGitHandler < Test::Unit::TestCase
  class << self
    def startup
      git = Git.init('/tmp/testrepo2')
      Dir.glob('**/resources/**/*.txt').each { |f| FileUtils.copy(f, '/tmp/testrepo2') }
      git.chdir do
        git.add('file1.txt')
        git.add('file2.txt')
        git.commit("added first two files\ntest1")
        git.add('file3.txt')
        git.add('file4.txt')
        git.commit("added two more files\ntest2")
        git.add_tag('version_latest', 'HEAD')
        git.add_tag('0.0.1', 'HEAD')
        git.add('file5.txt')
        git.commit('added yet another file')
      end
    end

    def shutdown
      FileUtils.remove_dir('/tmp/testrepo2/') if File.exist?('/tmp/testrepo2/')
    end
  end

  def test_git_last_version
    options = {}
    options[:git_path] = '/tmp/testrepo2'
    ver = Semantic::Version.new GitHandler.new(options).extract_last_version_from_tag
    assert { ver.major.zero? }
    assert { ver.minor.zero? }
    assert { ver.patch == 1 }
  rescue StandardError => _e
    assert { false }
  end

  def test_log_command
    git = Git.open('/tmp/testrepo2')
    Dir.glob('**/file6.txt').each { |f| FileUtils.copy(f, '/tmp/testrepo2') }
    git.chdir do
      git.add('file6.txt')
      git.commit('added first two files [bump_major]')
    end
    options = {}
    options[:git_path] = '/tmp/testrepo2'
    cmd = GitHandler.new(options).extract_command_from_log_message
    assert { cmd == GitCmdCode::BUMP_MAJOR }
  end
end
